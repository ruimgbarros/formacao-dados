---
title: "Jornalismo de Dados. Introdução, conceitos e como desenrascar umas coisas"
author: "Rui Barros"
output:
  xaringan::moon_reader:
    css: ["default", "xaringan-themer.css", "css/footer_plus.css"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: "16:9"
    seal: false
    includes:
      in_header: header.html
---

```{r setup, include=FALSE}


library(tidyverse)
library(xaringanthemer)
library(emo)

mono_accent(
    base_color         = "#000000",
    header_font_google = google_font("IBM Plex Sans", "700"),
    text_font_google   = google_font("IBM Plex Sans Condensed"),
    code_font_google   = google_font("IBM Plex Mono")
    )


```

layout: true

<div class="my-footer">
  <span>https://ruimgbarros.gitlab.io/formacao-dados</span>
</div> 

---
class: center, middle


background-image: url(figs/white_title.svg)
background-size: cover

<img src="figs/RR_Logo.svg" width="20%"/>

# Jornalismo de Dados
### Introdução, conceitos e como desenrascar <br> umas coisas
.large[**Rui Barros | Data Journalist**]


---
class: left

# Objectivos

--

- .large[Perceber a razão que me levava a demorar tanto a responder aos vossos pedidos.]

--

- .large[Tornar os jornalistas mais independentes na parte da análise de dados.]

--

- .large[Não chatear muito a Joana `r emo::ji("shrug")`.]

---
class: left

# O que não vamos ver aqui...

--

- .large[Coisas muito complexas com montes de linhas de código.]

--

- .large[Respostas imediatas a todas as vossas dúvidas.]

--

- .large[Coisas muito iterativas. Esta é uma formação de dados, não de desenvolvimento web.]


---
class: center

# Ferramentas que <br> vamos usar

<img src="figs/tools.png" height="400px"/>

---
class: center

# "The Pipeline"

<img src="figs/pipeline.png" height="400px"/>

---
class: center

# Some theory (sorry )

<h1 style="font-size:50pt; margin-top:140px">
`r emo::ji("sleep")`
`r emo::ji("sleep")`
`r emo::ji("sleep")`
</h1>


---
class: left

# Tipos de dados 

--

- .large[<b>Dados qualitativos:</b> É tudo o que se refere à qualidade de algo. Um atributo, uma qualidade, uma característica.]

--

### <center style="padding-top:50px"> Exemplos </center>

--
<center>
<b>Nomes nesta Sala:</b> Rui, Daniela, Inês, Joana, Maria, Rodrigo...
</center>

--

<center>
<b>Cor do cabelo:</b> Castanho, Loiro, Branco, Castanho, Castanho...
</center>

--

<center>
<b>Freg. em que vivem:</b> Nogueira, Benfica, NA, Campanhã, Ramalde...
</center>

---
class: left

# Tipos de dados 

--

- .large[<b>Dados quantitativos:</b> Números, números, números.]

--

### <center style="padding-top:50px"> Exemplos </center>

--
<center>
<b>Idades:</b> 18, 37, 40, 56, 90...
</center>

--

<center>
<b>Alturas:</b> 1.40m, 1.50m, 1.68m, 1.70m, 1.90m...
</center>

---
class: left

# Tipos de dados

- .large[<b>Dados ordinais:</b> É texto, mas dá para ordenar. Exemplo: Tamanhos de T-Shirts. É texto, mas há uma ordem.]

---
class: left

# Tipos de ficheiros de dados

--

- .large[<b>Sites:</b> <br> Os dados estão na net, mas não há bem forma de os trabalhar. Por exemplo, no [Ranking das Escolas](https://rr.sapo.pt/2019/02/16/pais/ranking-das-escolas-2018-veja-em-que-lugar-ficou-a-sua-escola/multimedia/141219/): nós conseguimos ver lá os dados, mas não os podemos descarregar nem fazer contas. <br> <span style="font-size:13pt">(Não é bem um ficheiro de dados, mas é para saberem que é onde estão os dados a maior parte das vezes)</span>]

--

- .large[<b>PDF:</b> <br> Têm muita informação valiosa, mas não são muito fixes para serem lidos como deve ser pelos computadores. Temos que fazer um processo de extracção.]


---
class: left

# Tipos de ficheiros de dados

- .large[<b>Excel:</b> <br> São brutais porque os dados já estão de alguma forma editáveis. Mas <br> 1) é preciso um programa proprietário para os abrir e ... <br> 2) às vezes estão carregados de "células lixo".]

--

- .large[<b>CSV:</b> <br> São o melhor amigo de um jornalista de dados. Basicamente são tabelas em que cada vírgula corresponde ao fim de uma célula e ao início de outra.]

---
class: left

# Tipos de ficheiros de dados

- .large[<b>JSON:</b> <br> Ultrapassam um bocado a lógica de tabela e assumem mais uma estrutura de dados em árvore. Já se tornam difíceis de ler para um humano, mas são muito mais fáceis de usar para quem está, por exemplo, a fazer o frontend.]

<div style="border-style: solid; border-width: 1px; padding: 30px; margin: 40px 10px 0px 10px">
  <h3>`r emo::ji("idea")` Coisas que devem saber: <b>API</b></h3>
  .medium[Uma API é uma forma de poderem requisitar dados a um serviço e ele devolve esses dados em json. <br> - [Exemplo 1](https://calm-beach-43103.herokuapp.com/echo?get_nome=rui): API para saber o género de um nome; <br> - [Exemplo 2](https://observador.pt/wp-json/wp/v2/posts): API com os textos do site do Observador; ]
</div>

---
class: center

# Ainda mais tipos de dados <br> (mais obscuros)

### mdb, gpx, geoJSON, SAV, TSV...

(juntem 3-4 letras e quase de certeza que há um formato de dados com esse nome)


---
class: center

# Como é que gostamos dos nossos dados?

<img src="https://ohi-science.org/data-science-training/img/tidy_data.png" width="100%">



---
class: center

# Let the hacking begin

<img src="https://miro.medium.com/proxy/1*cihPkdjv-7l7RqZdgI_BvA.gif" width="100%">

---
class: left, middle

# Recolher dados

---

class: left, middle

# Desafio 1:

.large[Base de dados com o índice médio de felicidade por nacionalidade. Mas a instituição que lança isto é idiota e só tem os dados em pdf.]

<center>
  <a href="https://gitlab.com/ruimgbarros/formacao-dados/-/raw/master/public/dados-felicidade.pdf?inline=false" target="_blank">
    <img src="https://thumbs.gfycat.com/ChubbyRegularEgret-size_restricted.gif" width="30%">
  </a>
</center>

---

class: middle, center

<a href="https://tabula.technology/" target="_blank">
  <img src ="https://www.windtopik.fr/wp-content/uploads/2019/01/tabula-logo-630x272.png">
</a>

---
class: left, middle

# Desafio 2:

.large[Queremos a lista das selecções que venceram o mundial desde que ele existe. Esses dados só existem na Wikipédia.]


<a href="https://pt.wikipedia.org/wiki/Copa_do_Mundo_FIFA" target="_blank">
  <center>
  <img src="https://labs.seoclick.com/wp-content/uploads/2012/04/wikipedia-seo.gif" width="10%">
  </center>
</a>
---

class: center, middle

<img src="https://i0.wp.com/www.jessicayung.com/wp-content/uploads/2016/08/google-sheets-logo.jpg?fit=680%2C400" width="50%">


---
class: left, middle

# Limpar dados

---
class: center, middle
<img src="https://pbs.twimg.com/media/ECrj0Q0W4AAgauv?format=png&name=small" height="100%">

---
class: left

# Lições de limpeza de dados

.large[O objectivo é que os dados deixem de ter "porcaria" de forma a que o computador os entenda como deve ser.]

- .large[Queremos que o texto seja texto e os números sejam números para o software que estamos a usar.]

- .large[Queremos que os campos sejam uniformes para poder filtrar as coisas como deve ser.]

---

# Tudo uma questão de `r emo::ji("eyes")` para a coisa

.large[Mais do que saber efectivamente limpar de forma mais simples possível (na pior das hipóteses, limpa-se à unha), é uma arte perceber logo o que é que há de errado ali.]

<div style="border-style: solid; border-width: 1px; padding: 30px; margin: 40px 10px 0px 10px">
  <h3>`r emo::ji("warning")` Uma limpeza mal feita é a morte de um trabalho</h3>
  .medium[Basta apenas uma célula estar "suja" para aquela célula não poder, por exemplo, ser ordenada. Imaginem o que é ordenar uma tabela com as despesas de um ministro e, a mais alta não estar a ser lida bem. A notícia fica automaticamente toda errada.]
</div>

---

class: middle, left

# Análise

---
class: middle

<blockquote>
<h2>Most of Data Science is filtering and counting things.</h2>
</blockquote>

---

class: left, center

# Filtros

<img src="https://lh3.googleusercontent.com/proxy/vV-OwXaLWR4p2HwnDIPYqeDVctFnev_T3AmipyPvIgZIcTb3gM4hQHeBVsj_6ObxdKihSu8y4pSB6o27G92fom43GlXxRurKdNaguDkg6TS497y1jFccH_u26LvxlG0HmA">

---
class: left, center

# Pivot Tables

<img src="https://www.nicepng.com/png/detail/630-6308794_pivot-table-icon-icon.png" width="30%">


---
class: inverse, center
background-image: url("https://media.giphy.com/media/xUOxfjsW9fWPqEWouI/giphy.gif")

# Questões?






---
class: left, middle

<img src="figs/RR_Icone.svg" width="15%"/>

# Obrigado! <br> (por tudo...)

<a href="https://twitter.com/rui_barros17"><i class="fa fa-twitter fa-fw"></i>&nbsp; @rui_barros17</a><br>
<a href="gitlab.com/ruimgbarros/"><i class="fa fa-gitlab fa-fw"></i>&nbsp; @ruimgbarros</a><br>
<a href="https://blog.ruimgbarros.com/" target='_blank'><i class="fa fa-link fa-fw"></i>&nbsp; blog.ruimgbarros.com </a><br>
<a href="mailto:ruimgbarros@gmail.com"><i class="fa fa-paper-plane fa-fw"></i>&nbsp; ruimgbarros@gmail.com</a>

Criado com [**remark.js**](http://remarkjs.com/) e o package de R [**xaringan**](https://github.com/yihui/xaringan)

